﻿using System;

namespace IsPalindrom
{
    class Program
    {
        static void Main()
        {
            if (IsPalindrome(Console.ReadLine()))
                Console.WriteLine("This Word is Palindrome");
            else
                Console.WriteLine("This Word isn't Palindrome");
        }

        static bool IsPalindrome(string Word)
        {
            int WordLength = Word.Length;
            for (int i = 0; i < WordLength; i++)
                if (Word[i] != Word[WordLength - i - 1])
                    return false;
            return true;
        }
    }
}
